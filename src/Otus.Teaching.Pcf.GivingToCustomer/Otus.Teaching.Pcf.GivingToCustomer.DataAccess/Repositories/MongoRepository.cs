﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Configuration;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRepository<T>
        : IRepository<T> where T : BaseEntity
    {
        private readonly MongoSettings _settings;

        protected readonly IMongoCollection<T> _mongoCollection;

        public MongoRepository(IMongoCollection<T> mongoCollection, IConfiguration configuration)
        {
            _mongoCollection = mongoCollection;
            _settings = configuration.GetSection(nameof(MongoSettings)).Get<MongoSettings>();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var list = await _mongoCollection.Find(c => true).ToListAsync();

            foreach (var entity in list)
            {
                if (entity is Customer customer)
                {
                    await LoadData(customer);
                }
            }

            return list;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _mongoCollection.Find(c => c.Id == id).FirstOrDefaultAsync();
            if (entity is Customer customer)
            {
                await LoadData(customer);
            }

            return entity;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var list = await _mongoCollection.Find(e => ids.Contains(e.Id)).ToListAsync();

            foreach (var entity in list)
            {
                if (entity is Customer customer)
                {
                    await LoadData(customer);
                }
            }

            return list;
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var entity = await _mongoCollection.Find(predicate).FirstOrDefaultAsync();
            if (entity is Customer customer)
            {
                await LoadData(customer);
            }

            return entity;
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var list = await _mongoCollection.Find(predicate).ToListAsync();

            foreach (var entity in list)
            {
                if (entity is Customer customer)
                {
                    await LoadData(customer);
                }
            }

            return list;
        }

        public async Task AddAsync(T entity)
        {
            await _mongoCollection.InsertOneAsync(entity);
            if (entity is PromoCode promoCode)
            {
                var customerCollection = _mongoCollection.Database
                    .GetCollection<Customer>(_settings.CustomerCollection);
                foreach (var customer in promoCode.Customers)
                {
                    var cust = await customerCollection
                        .Find(c => c.Id == customer.CustomerId)
                        .FirstOrDefaultAsync();
                    if (cust != null)
                    {
                        cust.PromoCodes.Add(
                            new PromoCodeCustomer()
                            {
                                CustomerId = cust.Id,
                                PromoCodeId = promoCode.Id
                            });
                        await customerCollection.ReplaceOneAsync(c => c.Id == cust.Id, cust);
                    }
                }
            }
        }

        public async Task UpdateAsync(T entity)
        {
            await _mongoCollection.ReplaceOneAsync(c => c.Id == entity.Id, entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _mongoCollection.DeleteOneAsync(c => c.Id == entity.Id);
        }

        private async Task LoadData(Customer customer)
        {
            if (customer != null)
            {
                if (customer.Preferences?.Count > 0)
                {
                    var ids = customer.Preferences.Select(c => c.PreferenceId);
                    var preferencies = await _mongoCollection.Database
                        .GetCollection<Preference>(_settings.PreferenceCollection)
                        .Find(e => ids.Contains(e.Id)).ToListAsync();
                    if (preferencies.Count > 0)
                    {
                        foreach (var preference in customer.Preferences)
                        {
                            preference.Customer = customer;
                            preference.CustomerId = customer.Id;
                            preference.Preference =
                                preferencies.FirstOrDefault(c => preference.PreferenceId == c.Id);
                        }
                    }
                }

                if (customer.PromoCodes?.Count > 0)
                {
                    var ids = customer.PromoCodes?.Select(c => c.PromoCodeId);

                    var promocodes = await _mongoCollection.Database
                        .GetCollection<PromoCode>(_settings.PromoCodeCollection)
                        .Find(e => ids.Contains(e.Id)).ToListAsync();
                    if (promocodes.Count > 0)
                    {
                        foreach (var promoCode in customer.PromoCodes)
                        {
                            promoCode.Customer = customer;
                            promoCode.CustomerId = customer.Id;
                            promoCode.PromoCode = promocodes.FirstOrDefault(c => promoCode.PromoCodeId == c.Id);
                        }
                    }
                }
            }
        }
    }
}