using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Configuration;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories;
using EfDbInitializer = Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data.EfDbInitializer;
using IDbInitializer = Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data.IDbInitializer;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddDataAccess(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repositories.EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddDbContext<DataContext>(x =>
            {
                //x.UseSqlite("Filename=PromocodeFactoryGivingToCustomerDb.sqlite");
                x.UseNpgsql(configuration.GetConnectionString("PromocodeFactoryGivingToCustomerDb"));
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });
            return services;
        }

        public static IServiceCollection AddMongoDb(this IServiceCollection services,
            IConfiguration configuration)
        {
            var settings =
                configuration.GetSection(nameof(MongoSettings)).Get<MongoSettings>();
            services
                .AddSingleton<IMongoClient>(_ =>
                    new MongoClient(configuration.GetConnectionString("MongoDB")))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoClient>()
                        .GetDatabase(settings.Database))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<Customer>(settings.CustomerCollection))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<Preference>(settings.PreferenceCollection))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<PromoCode>(settings.PromoCodeCollection))
                .AddScoped(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoClient>()
                        .StartSession());

            services.AddScoped<IDbInitializer, MongoDbInitializer>();
            services.AddScoped(typeof(IRepository<>), typeof(MongoRepository<>));


            return services;
        }
    }
}