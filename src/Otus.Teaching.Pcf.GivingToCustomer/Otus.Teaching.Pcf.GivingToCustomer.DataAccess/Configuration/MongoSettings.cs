﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Configuration
{
    public class MongoSettings
    {
        public string Database { get; set; }

        public string CustomerCollection { get; set; }

        public string PreferenceCollection { get; set; }

        public string PromoCodeCollection { get; set; }
    }
}