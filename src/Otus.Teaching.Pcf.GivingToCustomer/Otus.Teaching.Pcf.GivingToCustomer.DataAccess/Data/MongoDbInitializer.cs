﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Configuration;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly MongoSettings _settings;
        private readonly IMongoCollection<Customer> _customersCollection;
        private readonly IMongoCollection<Preference> _preferencesCollection;
        private readonly IMongoCollection<PromoCode> _promoCodesCollection;

        public MongoDbInitializer(IConfiguration configuration, IMongoCollection<Customer> customersCollection,
            IMongoCollection<Preference> preferencesCollection, IMongoCollection<PromoCode> promoCodesCollection)
        {
            _customersCollection = customersCollection;
            _preferencesCollection = preferencesCollection;
            _promoCodesCollection = promoCodesCollection;
            _settings =
                configuration.GetSection(nameof(MongoSettings)).Get<MongoSettings>();
        }

        public void InitializeDb()
        {
            _preferencesCollection.InsertMany(FakeDataFactory.Preferences);
            _customersCollection.InsertMany(FakeDataFactory.Customers);
            _promoCodesCollection.InsertMany(FakeDataFactory.PromoCodes);
        }

        public void RefreshDb()
        {
            var db = _preferencesCollection.Database;
            db.DropCollectionAsync(_settings.CustomerCollection);
            db.DropCollectionAsync(_settings.PreferenceCollection);
            db.DropCollectionAsync(_settings.PromoCodeCollection);
            _preferencesCollection.InsertMany(FakeDataFactory.Preferences);
            _customersCollection.InsertMany(FakeDataFactory.Customers);
            _promoCodesCollection.InsertMany(FakeDataFactory.PromoCodes);
        }
    }
}