﻿using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.AddRange(FakeDataFactory.PromoCodes);
            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();
        }

        public void RefreshDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.AddRange(FakeDataFactory.PromoCodes);
            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();
        }
    }
}