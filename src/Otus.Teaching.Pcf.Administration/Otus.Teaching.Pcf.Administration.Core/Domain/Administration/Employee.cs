﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [BsonIgnore] public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        [BsonIgnoreIfDefault] public Guid RoleId { get; set; }

        [BsonIgnoreIfNull] public virtual Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}