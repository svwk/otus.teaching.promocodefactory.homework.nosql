﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.Administration.Core.Domain
{
    public class BaseEntity
    {
        [BsonId] public Guid Id { get; set; }

        public BaseEntity() : this(Guid.NewGuid())
        {
        }

        public BaseEntity(Guid guid)
        {
            Id = guid;
        }
    }
}