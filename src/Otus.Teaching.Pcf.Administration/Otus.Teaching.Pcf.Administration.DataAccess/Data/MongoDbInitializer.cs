﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Configuration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Employee> _employeeCollection;
        private readonly IMongoCollection<Role> _roleCollection;
        private readonly MongoSettings settings;

        public MongoDbInitializer(
            IMongoCollection<Employee> employeesCollection,
            IMongoCollection<Role> rolesCollection, IConfiguration configuration)
        {
            _employeeCollection = employeesCollection;
            _roleCollection = rolesCollection;
            settings =
                configuration.GetSection(nameof(MongoSettings)).Get<MongoSettings>();
        }

        public void InitializeDb()
        {
            _employeeCollection.InsertMany(FakeDataFactory.Employees);
            _roleCollection.InsertMany(FakeDataFactory.Roles);
        }

        public void RefreshDb()
        {
            var db = _roleCollection.Database;
            db.DropCollectionAsync(settings.EmployeeCollection);
            db.DropCollectionAsync(settings.RoleCollection);
            _employeeCollection.InsertMany(FakeDataFactory.Employees);
            _roleCollection.InsertMany(FakeDataFactory.Roles);
        }
    }
}