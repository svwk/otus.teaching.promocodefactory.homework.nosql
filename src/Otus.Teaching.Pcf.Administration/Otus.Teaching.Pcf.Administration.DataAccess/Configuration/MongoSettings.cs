﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Configuration
{
    public class MongoSettings
    {
        public string Database { get; set; }

        public string EmployeeCollection { get; set; }

        public string RoleCollection { get; set; }
    }
}