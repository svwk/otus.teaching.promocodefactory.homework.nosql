﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T>
        : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _mongoCollection;

        public MongoRepository(IMongoCollection<T> mongoCollection)
        {
            _mongoCollection = mongoCollection;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _mongoCollection.Find(c => true).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _mongoCollection.Find(c => c.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _mongoCollection.Find(e => ids.Contains(e.Id)).ToListAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await _mongoCollection.Find(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await _mongoCollection.Find(predicate).ToListAsync();
        }

        public async Task AddAsync(T entity)
        {
            await _mongoCollection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await _mongoCollection.ReplaceOneAsync(c => c.Id == entity.Id, entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _mongoCollection.DeleteOneAsync(c => c.Id == entity.Id);
        }
    }
}